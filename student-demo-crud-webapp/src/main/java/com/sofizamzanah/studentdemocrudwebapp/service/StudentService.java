/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofizamzanah.studentdemocrudwebapp.service;
import com.sofizamzanah.studentdemocrudwebapp.dto.StudentDto;
import com.sofizamzanah.studentdemocrudwebapp.entity.Student;
import java.util.List;
import org.springframework.stereotype.Service;
/**
 *
 * @author ASUS
 */
@Service
public interface StudentService {
    
    public List<StudentDto> ambilDaftarStudent();
    public void perbaruiDataStudent(StudentDto studentDto);
    public void hapusDataStudent(Long studentId);
    public void simpanDataStudent(StudentDto studentDto);

    public StudentDto cariById(Long id);
}
