/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sofizamzanah.studentdemocrudwebapp.service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import com.sofizamzanah.studentdemocrudwebapp.dto.StudentDto;
import com.sofizamzanah.studentdemocrudwebapp.entity.Student;
import com.sofizamzanah.studentdemocrudwebapp.mapper.StudentMapper;
import com.sofizamzanah.studentdemocrudwebapp.repository.StudentRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS
 */
@Service
public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;
    
    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
    
    @Override
    public List<StudentDto> ambilDaftarStudent(){
        List<Student> students = this.studentRepository.findAll();
        // konversi obj student ke studentDto satu per satu dengan fungsi MAP pada Array
        List<StudentDto> studentDtos = students.stream()
                .map(student -> (StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());
        return studentDtos;
    }
    
    @Override
    public void hapusDataStudent(Long studentId){
        this.studentRepository.deleteById(studentId);
    }
    @Override
    public void perbaruiDataStudent( StudentDto studentDto ){
        Student student = StudentMapper.mapToStudent(studentDto);
        this.studentRepository.save(student);
    }
    @Override
    public void simpanDataStudent(StudentDto studentDto){
        Student student = StudentMapper.mapToStudent(studentDto);
        studentRepository.save(student);
    }

    @Override
    public StudentDto cariById(Long id) {
        Student student = studentRepository.findById(id).orElse(null);
        StudentDto studentDto = StudentMapper.mapToStudentDto(student);
        return studentDto;
    }
    
}
